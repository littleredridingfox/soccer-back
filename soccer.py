import os
from flask import (Flask, request, session, g, redirect, url_for, abort,
    render_template, flash, jsonify, json, make_response)
from werkzeug.contrib.fixers import ProxyFix
from flask_cors import CORS
from flask_session import Session
from flask_restful import Resource, Api, reqparse
import psycopg2 as pg
from psycopg2.extras import RealDictCursor, execute_values
import ast
from datetime import date
import dateutil.parser
from functools import wraps
import hashlib
# import json

DATABASE_URL = os.environ['DATABASE_URL']
SSLMODE = os.environ['SSLMODE']

app = Flask(__name__)
app.secret_key = b'\x9d\xa6]K\xe99\x1eO\xd4?\xd3mJ$\xf5\xd3'

# SESSION_TYPE = 'mongodb'
SESSION_TYPE = 'filesystem'

app.config.from_object(__name__)
Session(app)
cors = CORS(app, supports_credentials=True)
api = Api(app)

class CustomJSONEncoder(json.JSONEncoder):

    def default(self, obj):
        try:
            if isinstance(obj, date):
                return obj.isoformat()
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return json.JSONEncoder.default(self, obj)

app.json_encoder = CustomJSONEncoder

@api.representation('application/json')
def output_json(data, code, headers=None):
    resp = make_response(json.dumps(data), code)
    resp.headers.extend(headers or {})
    return resp

parser = reqparse.RequestParser()


def permission_required(permission_groups):
    def decorator(view):
        @wraps(view)
        def wrapper(*args, **kwargs):
            group = session.get("group", None)
            if group is None:
                return {"message": "Unauthorized"}, 401
            elif group not in permission_groups:
                return {"message": "You don't have permission to access this view"}, 403
            else:
                return view(*args, **kwargs)
        return wrapper
    return decorator

def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if 'db' not in g:
        g.db = pg.connect(DATABASE_URL, sslmode=SSLMODE)
        # g.db = pg.connect("dbname=soccer_db user=soccer_admin password=soccer_password")
        g.dbCursor = g.db.cursor(cursor_factory = RealDictCursor)
    return type('obj', (object,), {'db': g.db, 'cur': g.dbCursor})()

@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""
    db = g.pop('db', None)
    dbCursor = g.pop('dbCursor', None)
    if dbCursor is not None:
        dbCursor.close()
    if db is not None:
        db.close()



class HelloWorld(Resource):
    def get(self):
        
        args = parser.parse_args()
        return {
            "response": {
                "test": "Hello World!"
            }
        }

class UsersMapper():
    def get_password_by_username(username):
        cur = get_db().cur
        cur.execute(
            """
            SELECT passwordhash 
            FROM users 
            WHERE username = %(username)s
            """,
            {"username": username}
        )
        row = cur.fetchone() or [None]
        print(row)
        passwordhash = row["passwordhash"]
        if not passwordhash:
            raise ValueError("There's no user")
        # return row
        return passwordhash

    # def get_group_by_username(self, username):
    #     cur = get_db().cur
    #     cur.execute(
    #         """
    #             SELECT %(groups_table)s.name
    #             FROM %(groups_table)s INNER JOIN %(users_table)s on %(groups_table)s.id = %(users_table)s.group_id
    #             WHERE %(users_table)s.username = %(username)s;
    #         """,
    #         {"users_table": get_sql_table_name(Users), "groups_table": get_sql_table_name(Groups), "username": username}
    #     )
    #     group = cur.fetchone()[0]
    #     return group


class Team(Resource):
    def get(self, team_id):
        # abort_if_team_doesnt_exist(team_id)
        cur = get_db().cur

        cur.execute(
            "SELECT teams.*, coachs.first_name as coach_first_name, coachs.second_name as coach_second_name, coachs.last_name as coach_last_name, stadiums.capacity as stadium_capacity, stadiums.city as stadium_city, stadiums.name as stadium_name FROM teams INNER JOIN stadiums ON teams.stadium=stadiums.id INNER JOIN coachs on teams.coach=coachs.id WHERE teams.id=%(team_id)s;", 
            {'team_id': int(team_id)}
        )
        answer = cur.fetchone()
        # print(answer)
        return {
            "response": answer
        }
        # return TODOS[todo_id]

    @permission_required(["admin"])
    def delete(self, team_id):
        # abort_if_team_doesnt_exist(team_id)
        cur = get_db().cur
        cur.execute(
            "DELETE FROM teams WHERE id=%(team_id)s;COMMIT;", 
            {'team_id': int(team_id)}
        )

        return {
            "response": 'ok'
        }, 204

    @permission_required(["admin"])
    def put(self, team_id):
        cur = get_db().cur

        parser.add_argument('data', location='json', help='JSON data to update')
        args = parser.parse_args()
        data = ast.literal_eval(args["data"])

        updateColumns1 = ', '.join('%s' % key for key in data.keys())
        if (updateColumns1 == ""):
            abort(400)
        updateColumns2 = ', '.join('%s=data.%s' % (key, key) for key in data.keys())

        query = "UPDATE teams SET " + updateColumns2 + " FROM (VALUES %s) AS data ("+updateColumns1+", id) WHERE teams.id=data.id;COMMIT;"
        # return {'vals': list(data.values()), 'updateColumns1': updateColumns1, 'updateColumns2':updateColumns2, "query": query, "data": [tuple(data.values())+(player_id,)]}
        execute_values(cur, query, [tuple(data.values())+(int(team_id),)])

        parser.remove_argument('data')
        return {'status': 'ok'},200

class TeamsList(Resource):
    def get(self):
        # abort_if_team_doesnt_exist(team_id)
        cur = get_db().cur

        cur.execute(
            "SELECT teams.*, coachs.first_name as coach_first_name, coachs.second_name as coach_second_name, coachs.last_name as coach_last_name, stadiums.capacity as stadium_capacity, stadiums.city as stadium_city, stadiums.name as stadium_name FROM teams INNER JOIN stadiums ON teams.stadium=stadiums.id INNER JOIN coachs on teams.coach=coachs.id;"
        )

        answer = cur.fetchall()

        return {
            "response": {
                "items": answer,
                "count": len(answer)
            }
        }

    @permission_required(["admin"])
    def put(self):
        db = get_db()

        parser.add_argument('data', location='json', help='JSON data to update')
        args = parser.parse_args()
        data = ast.literal_eval(args["data"])

        db.cur.execute(
            "INSERT INTO teams (name, stadium, coach, city, prevseason) VALUES (%(team_name)s, %(team_stadium)s, %(team_coach)s, %(team_city)s, %(team_prevseason)s) RETURNING id;", 
            {
                'team_name': data["name"],
                'team_stadium': data["stadium"],
                'team_coach': data["coach"],
                'team_city': data["city"],
                'team_prevseason': data["prevseason"]
            }
        )
        answer = db.cur.fetchone()
        db.db.commit()
        # team = {'task': args['task']}

        parser.remove_argument('data')
        return answer, 201

class Player(Resource):
    def get(self, player_id):
        # abort_if_team_doesnt_exist(team_id)
        cur = get_db().cur

        cur.execute(
            "SELECT * FROM players WHERE id=%(player_id)s;", 
            {'player_id': int(player_id)}
        )
        answer = cur.fetchone()

        # print(answer)
        return {
            "response": answer
        }
        # return TODOS[todo_id]

    @permission_required(["admin"])
    def delete(self, player_id):
        # abort_if_team_doesnt_exist(team_id)
        cur = get_db().cur
        cur.execute(
            "DELETE FROM players WHERE id=%(player_id)s;COMMIT;", 
            {'player_id': int(player_id)}
        )

        return {
            "response": 'ok'
        }, 204

    @permission_required(["admin"])
    def put(self, player_id):
        cur = get_db().cur

        parser.add_argument('data', location='json', help='JSON data to update')
        args = parser.parse_args()
        data = ast.literal_eval(args["data"])

        updateColumns1 = ', '.join('%s' % key for key in data.keys())
        if (updateColumns1 == ""):
            abort(400)
        updateColumns2 = ', '.join('%s=data.%s' % (key, key) for key in data.keys())

        query = "UPDATE players SET " + updateColumns2 + " FROM (VALUES %s) AS data ("+updateColumns1+", id) WHERE players.id=data.id;COMMIT;"
        # return {'vals': list(data.values()), 'updateColumns1': updateColumns1, 'updateColumns2':updateColumns2, "query": query, "data": [tuple(data.values())+(player_id,)]}
        execute_values(cur, query, [tuple(data.values())+(int(player_id),)])

        parser.remove_argument('data')
        return {'status': 'ok'},200

class PlayersList(Resource):
    def get(self):
        # abort_if_team_doesnt_exist(team_id)
        cur = get_db().cur

        cur.execute(
            "SELECT * FROM players;"
        )
        answer = cur.fetchall()

        return {
            "response": {
                "items": answer,
                "count": len(answer)
            }
        }

    @permission_required(["admin"])
    def put(self):
        db = get_db()

        parser.add_argument('data', location='json', help='JSON data to update')
        args = parser.parse_args()
        data = ast.literal_eval(args["data"])
        # return data["tshirt"];
        db.cur.execute(
            "INSERT INTO players (first_name, last_name, second_name, team, number_in_team, tshirt) VALUES (%(first_name)s, %(last_name)s, %(second_name)s, %(team)s, %(number_in_team)s, %(tshirt)s) RETURNING id;", 
            data
        )
        answer = db.cur.fetchone()
        db.db.commit()

        parser.remove_argument('data')
        return answer, 201

class TeamPlayersList(Resource):
    def get(self, team_id):
        # abort_if_team_doesnt_exist(team_id)
        cur = get_db().cur

        cur.execute(
            "SELECT * FROM players WHERE team=%(team_id)s;",
            {'team_id': int(team_id)}
        )
        answer = cur.fetchall()

        return {
            "response": {
                "items": answer,
                "count": len(answer)
            }
        }

    # def put(self, team_id):
    #     db = get_db()

    #     parser.add_argument('data', location='json', help='JSON data to update')
    #     args = parser.parse_args()
    #     data = ast.literal_eval(args["data"])
    #     # return data["tshirt"];
    #     db.cur.execute(
    #         "INSERT INTO players (first_name, last_name, second_name, team, number_in_team, tshirt) VALUES (%(first_name)s, %(last_name)s, %(second_name)s, %(team)s, %(number_in_team)s, %(tshirt)s) RETURNING id;", 
    #         data
    #     )
    #     answer = db.cur.fetchone()
    #     db.db.commit()

    #     parser.remove_argument('data')
    #     return answer, 201

class Game(Resource):
    def get(self, game_id):
        # abort_if_team_doesnt_exist(game_id)
        cur = get_db().cur

        cur.execute(
            "SELECT games.*, t1.name as team1_name, t2.name as team2_name, stadiums.name as stadium_name, stadiums.capacity as stadium_capacity, coalesce(t1_goals.goals, 0) as team1_goals, coalesce(t2_goals.goals, 0) as team2_goals from games INNER JOIN teams as t1 ON games.team1 = t1.id INNER JOIN teams as t2 ON games.team2 = t2.id INNER JOIN stadiums ON games.stadium = stadiums.id LEFT JOIN ( SELECT games.id as game_id, COUNT(game_events.*) as goals from games INNER JOIN game_events ON game_events.game = games.id WHERE game_events.type = 'goal' AND game_events.by_team = games.team1 GROUP BY games.id ) t1_goals ON t1_goals.game_id = games.id LEFT JOIN ( SELECT games.id as game_id, COUNT(game_events.*) as goals from games INNER JOIN game_events ON game_events.game = games.id WHERE game_events.type = 'goal' AND game_events.by_team = games.team2 GROUP BY games.id ) t2_goals ON t2_goals.game_id = games.id WHERE games.id=%(game_id)s;", 
            {'game_id': int(game_id)}
        )
        answer = cur.fetchone()
        # print(answer)
        return {
            "response": answer
        }
        # return TODOS[todo_id]

    @permission_required(["admin"])
    def delete(self, game_id):
        # abort_if_team_doesnt_exist(game_id)
        cur = get_db().cur
        cur.execute(
            "DELETE FROM games WHERE id=%(game_id)s;COMMIT;", 
            {'game_id': int(game_id)}
        )

        return {
            "response": 'ok'
        }, 204

    @permission_required(["admin"])
    def put(self, game_id):
        cur = get_db().cur

        parser.add_argument('data', location='json', help='JSON data to update')
        args = parser.parse_args()
        data = ast.literal_eval(args["data"])

        # return dateutil.parser.parse(data["game_date"])

        if "game_date" in data:
            data["game_date"] = dateutil.parser.parse(data["game_date"])

        updateColumns1 = ', '.join('%s' % key for key in data.keys())
        if (updateColumns1 == ""):
            abort(400)
        updateColumns2 = ', '.join('%s=data.%s' % (key, key) for key in data.keys())

        query = "UPDATE games SET " + updateColumns2 + " FROM (VALUES %s) AS data ("+updateColumns1+", id) WHERE games.id=data.id;COMMIT;"
        # return {'vals': list(data.values()), 'updateColumns1': updateColumns1, 'updateColumns2':updateColumns2, "query": query, "data": [tuple(data.values())+(player_id,)]}
        execute_values(cur, query, [tuple(data.values())+(int(game_id),)])

        parser.remove_argument('data')
        return {'status': 'ok'},200

class GamesList(Resource):
    def get(self):
        # abort_if_team_doesnt_exist(team_id)
        cur = get_db().cur

        cur.execute(
            "SELECT games.*, t1.name as team1_name, t2.name as team2_name, stadiums.name as stadium_name, stadiums.capacity as stadium_capacity, coalesce(t1_goals.goals, 0) as team1_goals, coalesce(t2_goals.goals, 0) as team2_goals from games INNER JOIN teams as t1 ON games.team1 = t1.id INNER JOIN teams as t2 ON games.team2 = t2.id INNER JOIN stadiums ON games.stadium = stadiums.id LEFT JOIN ( SELECT games.id as game_id, COUNT(game_events.*) as goals from games INNER JOIN game_events ON game_events.game = games.id WHERE game_events.type = 'goal' AND game_events.by_team = games.team1 GROUP BY games.id ) t1_goals ON t1_goals.game_id = games.id LEFT JOIN ( SELECT games.id as game_id, COUNT(game_events.*) as goals from games INNER JOIN game_events ON game_events.game = games.id WHERE game_events.type = 'goal' AND game_events.by_team = games.team2 GROUP BY games.id ) t2_goals ON t2_goals.game_id = games.id;"
        )

        answer = cur.fetchall()

        return {
            "response": {
                "items": answer,
                "count": len(answer)
            }
        }

    @permission_required(["admin"])
    def put(self):
        db = get_db()

        parser.add_argument('data', location='json', help='JSON data to update')
        args = parser.parse_args()
        data = ast.literal_eval(args["data"])

        if "game_date" in data:
            data["game_date"] = dateutil.parser.parse(data["game_date"])

        db.cur.execute(
            "INSERT INTO games (team1, stadium, team2, tour, game_date) VALUES (%(game_team1)s, %(game_stadium)s, %(game_team2)s, %(game_tour)s, %(game_game_date)s) RETURNING id;", 
            {
                'game_team1': data["team1"],
                'game_stadium': data["stadium"],
                'game_team2': data["team2"],
                'game_tour': data["tour"],
                'game_game_date': data["game_date"]
            }
        )
        answer = db.cur.fetchone()
        db.db.commit()
        # team = {'task': args['task']}

        parser.remove_argument('data')
        return answer, 201

class GameEventsList(Resource):
    def get(self, game_id):
        # abort_if_team_doesnt_exist(team_id)
        cur = get_db().cur

        cur.execute(
            "SELECT game_events.*, p1.first_name as by_player_first_name, p1.second_name as by_player_second_name, p1.last_name as by_player_last_name, p1.tshirt as by_player_tshirt, t1.name as by_team_name FROM game_events INNER JOIN players as p1 on p1.id = game_events.by_player INNER JOIN teams as t1 on t1.id = game_events.by_team WHERE game=%(game_id)s;",
            {'game_id': int(game_id)}
        )
        answer = cur.fetchall()

        return {
            "response": {
                "items": answer,
                "count": len(answer)
            }
        }

    @permission_required(["admin"])
    def put(self, game_id):
        db = get_db()

        parser.add_argument('data', location='json', help='JSON data to update')
        args = parser.parse_args()
        data = ast.literal_eval(args["data"])
        # return data["tshirt"];
        db.cur.execute(
            "INSERT INTO game_events (game, by_player, by_team, to_player, to_team, type, minute) VALUES (%(game)s, %(by_player)s, %(by_team)s, %(to_player)s, %(to_team)s, %(type)s, %(minute)s) RETURNING id;", 
            data
        )
        answer = db.cur.fetchone()
        db.db.commit()

        parser.remove_argument('data')
        return answer, 201

class Stadium(Resource):
    def get(self, stadium_id):
        # abort_if_team_doesnt_exist(stadium_id)
        cur = get_db().cur

        cur.execute(
            "SELECT * from stadiums where id=%(stadium_id)s;", 
            {'stadium_id': int(stadium_id)}
        )
        answer = cur.fetchone()
        # print(answer)
        return {
            "response": answer
        }
        # return TODOS[todo_id]

    @permission_required(["admin"])
    def delete(self, stadium_id):
        # abort_if_team_doesnt_exist(stadium_id)
        cur = get_db().cur
        cur.execute(
            "DELETE FROM stadiums WHERE id=%(stadium_id)s;COMMIT;", 
            {'stadium_id': int(stadium_id)}
        )

        return {
            "response": 'ok'
        }, 204

    @permission_required(["admin"])
    def put(self, stadium_id):
        cur = get_db().cur

        parser.add_argument('data', location='json', help='JSON data to update')
        args = parser.parse_args()
        data = ast.literal_eval(args["data"])

        # return dateutil.parser.parse(data["game_date"])

        if "game_date" in data:
            data["game_date"] = dateutil.parser.parse(data["game_date"])

        updateColumns1 = ', '.join('%s' % key for key in data.keys())
        if (updateColumns1 == ""):
            abort(400)
        updateColumns2 = ', '.join('%s=data.%s' % (key, key) for key in data.keys())

        query = "UPDATE stadiums SET " + updateColumns2 + " FROM (VALUES %s) AS data ("+updateColumns1+", id) WHERE stadiums.id=data.id;COMMIT;"
        # return {'vals': list(data.values()), 'updateColumns1': updateColumns1, 'updateColumns2':updateColumns2, "query": query, "data": [tuple(data.values())+(player_id,)]}
        execute_values(cur, query, [tuple(data.values())+(int(stadium_id),)])

        parser.remove_argument('data')
        return {'status': 'ok'},200

class StadiumsList(Resource):
    def get(self):
        # abort_if_team_doesnt_exist(team_id)
        cur = get_db().cur

        cur.execute(
            "SELECT * from stadiums;"
        )

        answer = cur.fetchall()

        return {
            "response": {
                "items": answer,
                "count": len(answer)
            }
        }

    @permission_required(["admin"])
    def put(self):
        db = get_db()

        parser.add_argument('data', location='json', help='JSON data to update')
        args = parser.parse_args()
        data = ast.literal_eval(args["data"])

        db.cur.execute(
            "INSERT INTO stadiums (capacity, city, name) VALUES (%(capacity)s, %(city)s, %(name)s) RETURNING id;", 
            data
        )
        answer = db.cur.fetchone()
        db.db.commit()
        # team = {'task': args['task']}

        parser.remove_argument('data')
        return answer, 201

class Coach(Resource):
    def get(self, coach_id):
        # abort_if_team_doesnt_exist(coach_id)
        cur = get_db().cur

        cur.execute(
            "SELECT * from coachs where id=%(coach_id)s;", 
            {'coach_id': int(coach_id)}
        )
        answer = cur.fetchone()
        # print(answer)
        return {
            "response": answer
        }
        # return TODOS[todo_id]

    @permission_required(["admin"])
    def delete(self, coach_id):
        # abort_if_team_doesnt_exist(coach_id)
        cur = get_db().cur
        cur.execute(
            "DELETE FROM coachs WHERE id=%(coach_id)s;COMMIT;", 
            {'coach_id': int(coach_id)}
        )

        return {
            "response": 'ok'
        }, 204

    @permission_required(["admin"])
    def put(self, coach_id):
        cur = get_db().cur

        parser.add_argument('data', location='json', help='JSON data to update')
        args = parser.parse_args()
        data = ast.literal_eval(args["data"])

        # return dateutil.parser.parse(data["game_date"])

        if "game_date" in data:
            data["game_date"] = dateutil.parser.parse(data["game_date"])

        updateColumns1 = ', '.join('%s' % key for key in data.keys())
        if (updateColumns1 == ""):
            abort(400)
        updateColumns2 = ', '.join('%s=data.%s' % (key, key) for key in data.keys())

        query = "UPDATE coachs SET " + updateColumns2 + " FROM (VALUES %s) AS data ("+updateColumns1+", id) WHERE coachs.id=data.id;COMMIT;"
        # return {'vals': list(data.values()), 'updateColumns1': updateColumns1, 'updateColumns2':updateColumns2, "query": query, "data": [tuple(data.values())+(player_id,)]}
        execute_values(cur, query, [tuple(data.values())+(int(coach_id),)])

        parser.remove_argument('data')
        return {'status': 'ok'},200

class CoachsList(Resource):
    def get(self):
        # abort_if_team_doesnt_exist(team_id)
        cur = get_db().cur

        cur.execute(
            "SELECT * from coachs;"
        )

        answer = cur.fetchall()

        return {
            "response": {
                "items": answer,
                "count": len(answer)
            }
        }

    @permission_required(["admin"])
    def put(self):
        db = get_db()

        parser.add_argument('data', location='json', help='JSON data to update')
        args = parser.parse_args()
        data = ast.literal_eval(args["data"])

        db.cur.execute(
            "INSERT INTO coachs (first_name, second_name, last_name) VALUES (%(first_name)s, %(second_name)s, %(last_name)s) RETURNING id;", 
            data
        )
        answer = db.cur.fetchone()
        db.db.commit()
        # team = {'task': args['task']}

        parser.remove_argument('data')
        return answer, 201

class TeamGamesList(Resource):
    def get(self, team_id):
        # abort_if_team_doesnt_exist(team_id)
        cur = get_db().cur

        cur.execute(
            "SELECT games.*, t1.name as team1_name, t2.name as team2_name, stadiums.name as stadium_name, stadiums.capacity as stadium_capacity, coalesce(t1_goals.goals, 0) as team1_goals, coalesce(t2_goals.goals, 0) as team2_goals from games INNER JOIN teams as t1 ON games.team1 = t1.id INNER JOIN teams as t2 ON games.team2 = t2.id INNER JOIN stadiums ON games.stadium = stadiums.id LEFT JOIN ( SELECT games.id as game_id, COUNT(game_events.*) as goals from games INNER JOIN game_events ON game_events.game = games.id WHERE game_events.type = 'goal' AND game_events.by_team = games.team1 GROUP BY games.id ) t1_goals ON t1_goals.game_id = games.id LEFT JOIN ( SELECT games.id as game_id, COUNT(game_events.*) as goals from games INNER JOIN game_events ON game_events.game = games.id WHERE game_events.type = 'goal' AND game_events.by_team = games.team2 GROUP BY games.id ) t2_goals ON t2_goals.game_id = games.id WHERE games.team1 = %(team_id)s OR games.team2 = %(team_id)s;",
            {'team_id': int(team_id)}
        )
        answer = cur.fetchall()

        return {
            "response": {
                "items": answer,
                "count": len(answer)
            }
        }

class Login(Resource):
    def post(self):
        credentials = request.json
        password = UsersMapper.get_password_by_username(credentials["username"])
        if password == hashlib.sha512(credentials["password"].encode('utf-8')).hexdigest():
            session['logged_in'] = True
            session['group'] = "admin"
            session.permanent = True
            return {"group": session.get('group')}, 200
            # return {"auth": "ok"}, 200
        else:
            return None, 403

class Logout(Resource):

    def post(self):
        session['logged_in'] = False
        session['group'] = None
        return None, 200

class AuthCheck(Resource):
    def get(self):
        return {
            "logged_in": session['logged_in'],
            "group": session['group']
        }

api.add_resource(HelloWorld, '/')
api.add_resource(TeamsList, '/teams')
api.add_resource(Team, '/teams/<team_id>')
api.add_resource(TeamGamesList, '/teams/<team_id>/games')
api.add_resource(PlayersList, '/players')
api.add_resource(Player, '/players/<player_id>')
api.add_resource(TeamPlayersList, '/teams/<team_id>/players')
api.add_resource(GamesList, '/games')
api.add_resource(Game, '/games/<game_id>')
api.add_resource(GameEventsList, '/games/<game_id>/events')
api.add_resource(StadiumsList, '/stadiums')
api.add_resource(Stadium, '/stadiums/<stadium_id>')
api.add_resource(CoachsList, '/coachs')
api.add_resource(Coach, '/coachs/<coach_id>')
api.add_resource(Login, '/auth/login')
api.add_resource(Logout, '/auth/logout')
api.add_resource(AuthCheck, '/auth/check')


app.wsgi_app = ProxyFix(app.wsgi_app)

if __name__ == "__main__":
    app.config['DEBUG'] = True
    app.run()
